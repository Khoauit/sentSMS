package sendSMS;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

public class SMSServices {

	final static String APIKey = "8B969D32D415660CBAB499CBDCDACF";// Dang ky tai khoan tai esms.vn de lay Key
	final static String SecretKey = "76B3FBD391239D5805E2CEB3C51326";
	final static int SmsType = 2;
	final static String BrandName = "QCAO_ONLINE";
	private String message;
	private String phone;

	/**
	 * 
	 */
	public SMSServices() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	
	
	public String sendGetXML() throws IOException {

		String url = "http://rest.esms.vn/MainService.svc/xml/SendMultipleMessage_V4_get?ApiKey="
				+ URLEncoder.encode(APIKey, "UTF-8") + "&SecretKey=" + URLEncoder.encode(SecretKey, "UTF-8") + "&Phone="
				+ URLEncoder.encode(phone, "UTF-8") + "&Content=" + URLEncoder.encode(message, "UTF-8");

		switch (SmsType) {
		case 2:
			url += "&SmsType=2&Brandname=" + URLEncoder.encode(BrandName, "UTF-8");
			break;
		case 8:
			url += "&SmsType=8";
			break;
		default:
			url += "&SmsType=3";
			break;
		}

		// De dang ky brandname rieng vui long lien he hotline 0902435340 hoac nhan vien
		// kinh Doanh cua ban
		URL obj;
		try {
			obj = new URL(url);

			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			// you need to encode ONLY the values of the parameters

			con.setRequestMethod("GET");

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);
			if (responseCode == 200)// Đã gọi URL thành công, tuy nhiên bạn phải tự kiểm tra CodeResult xem tin nhắn
									// có gửi thành công không, vì có thể tài khoản bạn không đủ tiền thì sẽ thất
									// bại
			{
				// Check CodeResult from response
			}
			// Đọc Response
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			System.out.println(response.toString());
			Document document = loadXMLFromString(response.toString());
			document.getDocumentElement().normalize();
			System.out.println("Root element :" + document.getDocumentElement().getNodeName());
			Node node = document.getElementsByTagName("CodeResult").item(0);
			System.out.println("CodeResult: " + node.getTextContent());
			node = document.getElementsByTagName("SMSID").item(0);
			if (node != null) {
				System.out.println("SMSID: " + node.getTextContent());
				return "SMSID: " + node.getTextContent();
			} else {
				node = document.getElementsByTagName("ErrorMessage").item(0);
				System.out.println("ErrorMessage: " + node.getTextContent());
				return "ErrorMessage: " + node.getTextContent();
			}
			// document.getElementsByTagName("CountRegenerate").item(0).va
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "FAILED";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "FAILED";
		}
		// return "SUCCESS";

	}

	public static String sendGetJSON(String phone , String message) throws IOException {

		String url = "http://rest.esms.vn/MainService.svc/json/SendMultipleMessage_V4_get?ApiKey="
				+ URLEncoder.encode(APIKey, "UTF-8") + "&SecretKey=" + URLEncoder.encode(SecretKey, "UTF-8") + "&Phone="
				+ URLEncoder.encode(phone, "UTF-8") + "&Content=" + URLEncoder.encode(message, "UTF-8");

		switch (SmsType) {
		case 2:
			url += "&SmsType=2&Brandname=" + URLEncoder.encode(BrandName, "UTF-8");
			break;
		case 8:
			url += "&SmsType=8";
			break;
		default:
			url += "&SmsType=3";
			break;
		}

		URL obj;
		try {
			obj = new URL(url);

			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("GET");
			con.setRequestProperty("Accept", "application/json");

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);
			if (responseCode == 200)// Đã gọi URL thành công, tuy nhiên bạn phải tự kiểm tra CodeResult xem tin nhắn
									// có gửi thành công không, vì có thể tài khoản bạn không đủ tiền thì sẽ thất
									// bại
			{
				// Check CodeResult from response
			}
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			JSONObject json = (JSONObject) new JSONParser().parse(response.toString());
			System.out.println("CodeResult=" + json.get("CodeResult"));
			System.out.println("SMSID=" + json.get("SMSID"));
			System.out.println("ErrorMessage=" + json.get("ErrorMessage"));
			if (json.get("SMSID") != null) {
				return "SMSID: " + json.get("SMSID");
			} else {
				return "ErrorMessage: " + json.get("ErrorMessage");
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return "FAILED";
		} catch (Exception e) {
			e.printStackTrace();
			return "FAILED";
		}
		// return "SUCCESS";

	}

	public Document loadXMLFromString(String xml) throws Exception {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		InputSource is = new InputSource(new StringReader(xml));
		return builder.parse(is);
	}
	
}
