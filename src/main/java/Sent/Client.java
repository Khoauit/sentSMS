package Sent;

public class Client {
private String name;
private String phone;
private String email;
private String loan_id;
public Client(String name, String phone, String email, String loan_id) {
	this.name = name;
	this.phone = phone;
	this.email = email;
	this.loan_id=loan_id;
}
public Client() {
	
}
public String getLoan_id() {
	return loan_id;
}
public void setLoan_id(String loan_id) {
	this.loan_id = loan_id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getPhone() {
	return phone;
}
public void setPhone(String phone) {
	this.phone = phone;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}

}
